export class Ingredient {
    constructor (public name:string, public amount:number) {}
}


// ABOVE TYPESCRIPT IS A SHORTCUT FOR FOLLOWING JS:

// public name: string;
// public amount: number;

// constructor (name: string, amount: number) {
//     this.name = name;
//     this.amount = amount;
// }