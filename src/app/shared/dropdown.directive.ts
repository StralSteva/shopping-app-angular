import { Directive, HostListener, HostBinding } from "@angular/core";
import { callLifecycleHooksChildrenFirst } from "@angular/core/src/view/provider";

@Directive({
    // attribute selector using []
    selector: '[appDropdown]'
})
export class DropdownDirective {
    // adding directives:
    // Host binding is for attach or detech css class
    @HostBinding('class.open') isOpen = false;
    // After this, add DropdownDirective to ngModule in app.module

    // apply dropdown class once clicked and remove dropdown by another click
    // use OPEN method
    @HostListener('click') toggleOpen() {
        this.isOpen = !this.isOpen;
    }
}