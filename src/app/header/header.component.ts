// #2 import component to be recognised by angylar
import { Component } from "@angular/core";

// #3 pass js object to decorator to configure component 
// if you do this #2 , above will be created manually
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})

// #1 create class
export class HeaderComponent {
    // @Output() featureSelected = new EventEmitter<string>();

    // onSelect(feature:string) {
    //     this.featureSelected.emit(feature);
    // }
}

// #4 add headercompoennt to app.module.ts !