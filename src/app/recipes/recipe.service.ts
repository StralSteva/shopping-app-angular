import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

//After this, can do constructor!
@Injectable()
export class RecipeService {
    recipeSelected = new EventEmitter<Recipe>();
    
    // with private you cant access recipe from outsite
    private recipes: Recipe[] = [
        // in order to see this add template to recipe-list.html!
        new Recipe(
            'A Test Recipe', 
            'Simple test', 
            'https://images.media-allrecipes.com/userphotos/560x315/4507666.jpg',
            [
                new Ingredient('Lemon', 1),
                new Ingredient('Cores in g', 500)
            ]),
        new Recipe(
            'Another Recipe', 
            '2nd Test', 
            'https://images.media-allrecipes.com/userphotos/560x315/4507666.jpg',
            [
                new Ingredient('Nuts',20),
                new Ingredient('Honey', 1)
            ]) 
    ];
    
    constructor(private slService: ShoppingListService) {}

    // this allows accessing recipes from outside
    // we use slice() to get only copy and not to change obj. recipes
    getRecipes() {
        return this.recipes.slice();
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    
}