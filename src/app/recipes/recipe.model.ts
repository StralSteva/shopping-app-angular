import { Ingredient } from "../shared/ingredient.model";

// THIS IS VANILLA TYPESCRIPT with NO @DECORATOR
export class Recipe {
    // name can be accessed
    public name: string;
    public description: string;
    public imagePath: string;
    public ingredients: Ingredient[];

    // extentiate this model blueprint with keyword
    constructor(name:string, desc:string, imagePath:string, ingredients: Ingredient[]) {
        this.name = name;
        this.description = desc;
        // img path with bypass security!
        this.imagePath = imagePath;
        this.ingredients = ingredients;
        
    }
}


